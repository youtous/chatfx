package chatfx;

import client.Connection;
import client.Event;
import client.TextClient;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.*;


public class ChatController implements Observer {

    @FXML
    private VBox messageArea;

    @FXML
    private TextField inputField;

    @FXML
    private ListView clientsList;

    private Connection connection = null;

    /**
     * Contain the ignored clients
     */
    private ArrayList<String> ignoredClients = new ArrayList<>();
    /**
     * Contain the clients that ignore us
     */
    private ArrayList<String> ignoredByClients = new ArrayList<>();

    @Override
    public void update(Observable o, Object arg) {
        Platform.runLater(() -> {

            Event event = (Event) arg;
            TextArea text = new TextArea();
            text.setEditable(false);
            text.getStyleClass().add("text-area-textflow");

            text.setWrapText(true);

            Stage stage = ((Stage) ChatController.this.inputField.getScene().getWindow());


            switch (event.type) {
                case CONNECTION_ENDED:
                    text.setStyle("-fx-text-fill: blue;");
                    text.setText("*** Vous êtes maintenant déconnecté ***");
                    System.exit(0);
                case MESSAGE:
                    String user = event.data[0].split(">")[0];
                    if (!ChatController.this.ignoredClients.contains(user)) {
                        text.setText(event.data[0]);
                    }
                    break;
                case ALIAS_UPDATED:
                    stage.setTitle(event.data[0]);
                    ChatController.this.inputField.setPromptText(event.data[0]);

                    text.setStyle("-fx-text-fill: blue;");
                    text.setText("*** Vous êtes maintenant connecté avec le pseudonyme " + event.data[0] + " ***");
                    ChatController.this.connection.sendToServer("/list");
                    break;
                case ERROR:
                    text.setStyle("-fx-text-fill: red;");
                    text.setText("ERREUR - " + event.data[0]);
                    break;
                case USER_CONNECTED:
                    text.setStyle("-fx-text-fill: orange;");
                    text.setText("[" + event.data[0] + "] s'est connecté.");
                    ChatController.this.clientsList.getItems().add(event.data[0]);
                    break;
                case USER_DISCONNECTED:
                    text.setStyle("-fx-text-fill: orange;");
                    text.setText("[" + event.data[0] + "] s'est déconnecté.");
                    ChatController.this.clientsList.getItems().remove(event.data[0]);
                    break;
                case LIST:
                    text.setStyle("-fx-text-fill: blue;");

                    String[] clients = event.data[0].split(" ");
                    for (int i = 0; i < clients.length; i++) {
                        String username = clients[i];
                        if (ChatController.this.ignoredClients.contains(username)) {
                            clients[i] = "(" + clients[i] + ")";
                        }
                        if (ChatController.this.ignoredByClients.contains(username)) {
                            clients[i] = "[" + clients[i] + "]";
                        }
                    }

                    text.setText("*** Clients connectés : " + String.join(" ", clients) + " ***");
                    ChatController.this.clientsList.getItems().setAll(clients);
                    break;
                case USER_RENAMED:
                    text.setStyle("-fx-text-fill: orange;");
                    text.setText("[" + event.data[0] + "] est maintenant connu sous : " + event.data[1]);
                    ChatController.this.clientsList.getItems().remove(event.data[0]);
                    ChatController.this.clientsList.getItems().add(event.data[1]);
                    break;
                case PRIVATE:
                    if (!this.ignoredClients.contains(event.data[0])) {
                        TextInputDialog dialog = new TextInputDialog();

                        // disable the icon
                        dialog.initStyle(StageStyle.UTILITY);
                        dialog.setGraphic(null);

                        // custom submit button
                        ButtonType sendButton = new ButtonType("Répondre", ButtonBar.ButtonData.OK_DONE);
                        ButtonType ok = new ButtonType("Ok", ButtonBar.ButtonData.CANCEL_CLOSE);
                        dialog.getDialogPane().getButtonTypes().setAll(ok, sendButton);

                        dialog.setTitle("Nouveau message de : " + event.data[0]);
                        dialog.setHeaderText(String.join(" ", Arrays.copyOfRange(event.data, 1, event.data.length)));
                        dialog.setContentText("Réponse");
                        Optional<String> result = dialog.showAndWait();

                        if (result.isPresent() && result.get().length() > 0) {
                            ChatController.this.connection.sendToServer("/private " + event.data[0] + " " + result.get());
                        }

                        ChatController.this.clientsList.getSelectionModel().clearSelection();
                        dialog.hide();
                    }
                    break;
                case IGNORE:
                    if (!ChatController.this.ignoredClients.contains(event.data[0])) {
                        text.setStyle("-fx-text-fill: orange;");
                        text.setText("*** " + event.data[0] + " est désormais ignoré ***");
                        ChatController.this.ignoredClients.add(event.data[0]);

                        // list updating
                        String username = event.data[0];
                        int indexOfItemInList;
                        if (ChatController.this.ignoredByClients.contains(username)) {
                            indexOfItemInList = ChatController.this.clientsList.getItems().indexOf("[" + event.data[0] + "]");
                            username = "[(" + username + ")]";
                        } else {
                            indexOfItemInList = ChatController.this.clientsList.getItems().indexOf(event.data[0]);
                            username = "(" + username + ")";
                        }

                        ChatController.this.clientsList.getItems().set(indexOfItemInList, username);

                    } else {
                        text.setStyle("-fx-text-fill: orange;");
                        text.setText("*** " + event.data[0] + " est déjà ignoré ***");
                    }
                    break;
                case IGNORED_BY:
                    if (!ChatController.this.ignoredByClients.contains(event.data[0])) {
                        ChatController.this.ignoredByClients.add(event.data[0]);
                        text.setStyle("-fx-text-fill: orange;");
                        text.setText("*** " + event.data[0] + " vous a ignoré ***");

                        // list updating
                        String username = event.data[0];
                        int indexOfItemInList;
                        if (ChatController.this.ignoredClients.contains(username)) {
                            indexOfItemInList = ChatController.this.clientsList.getItems().indexOf("(" + event.data[0] + ")");
                            username = "[(" + username + ")]";
                        } else {
                            indexOfItemInList = ChatController.this.clientsList.getItems().indexOf(event.data[0]);
                            username = "[" + username + "]";
                        }

                        ChatController.this.clientsList.getItems().set(indexOfItemInList, username);
                    }
                    break;
                case UNIGNORE:
                    if (ChatController.this.ignoredClients.contains(event.data[0])) {
                        ChatController.this.ignoredClients.remove(event.data[0]);
                        text.setStyle("-fx-text-fill: orange;");
                        text.setText("*** " + event.data[0] + " est désormais dé-ignoré ***");

                        // list updating
                        String username = event.data[0];
                        int indexOfItemInList;
                        if (ChatController.this.ignoredByClients.contains(username)) {
                            indexOfItemInList = ChatController.this.clientsList.getItems().indexOf("[(" + event.data[0] + ")]");
                            username = "[" + username + "]";
                        } else {
                            indexOfItemInList = ChatController.this.clientsList.getItems().indexOf("(" + event.data[0] + ")");
                        }

                        ChatController.this.clientsList.getItems().set(indexOfItemInList, username);

                    } else {
                        text.setStyle("-fx-text-fill: orange;");
                        text.setText("*** " + event.data[0] + " n'est pas ignoré ***");
                    }

                    break;
                case UNIGNORED_BY:
                    if (ChatController.this.ignoredByClients.contains(event.data[0])) {
                        ChatController.this.ignoredByClients.remove(event.data[0]);

                        text.setStyle("-fx-text-fill: orange;");
                        text.setText("*** " + event.data[0] + " vous a dé-ignoré ***");

                        // list updating
                        String username = event.data[0];
                        int indexOfItemInList;
                        if (ChatController.this.ignoredClients.contains(username)) {
                            indexOfItemInList = ChatController.this.clientsList.getItems().indexOf("[(" + event.data[0] + ")]");
                            username = "(" + username + ")";
                        } else {
                            indexOfItemInList = ChatController.this.clientsList.getItems().indexOf("[" + event.data[0] + "]");
                        }

                        ChatController.this.clientsList.getItems().set(indexOfItemInList, username);
                    }
                    break;
            }

            // prevent empty text
            if (text.getText().length() > 0) {
                // from https://stackoverflow.com/questions/15593287/binding-textarea-height-to-its-content#15602005
                SimpleIntegerProperty count = new SimpleIntegerProperty(20);
                text.setMinHeight(count.get());

                text.prefHeightProperty().bindBidirectional(count);
                text.minHeightProperty().bindBidirectional(count);

                text.scrollTopProperty().addListener((ov, old, newval) -> {
                    count.setValue(count.get() + newval.intValue());

                });
                ChatController.this.messageArea.getChildren().add(text);
            }
        });
    }

    public void onSend() {
        if (this.inputField.getText().length() != 0) {
            if (this.connection == null) {
                try {
                    this.connection = new Connection("localhost", 3333, inputField.getText(), new TextClient());
                    this.connection.addObserver(this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                this.connection.sendToServer(inputField.getText());
            }
            this.inputField.setText("");
        }
    }

    public void init() {
        ChatController.this.clientsList.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            // prevent null input dialog (when unselected)
            // prevent ignore or ignoreBy dialog
            String username = (String) newValue;
            if (username != null && username.charAt(0) != '(' && username.charAt(0) != '[') {
                Platform.runLater(() -> {
                    TextInputDialog dialog = new TextInputDialog();

                    // disable the icon
                    dialog.initStyle(StageStyle.UTILITY);
                    dialog.setGraphic(null);

                    // custom submit button
                    ButtonType sendButton = new ButtonType("Envoyer", ButtonBar.ButtonData.OK_DONE);
                    dialog.getDialogPane().getButtonTypes().setAll(ButtonType.CANCEL, sendButton);

                    dialog.setTitle("Envoyer un message à " + username);
                    dialog.setHeaderText("Nouveau message privé");
                    Optional<String> result = dialog.showAndWait();

                    if (result.isPresent() && result.get().length() > 0) {
                        ChatController.this.connection.sendToServer("/private " + username + " " + result.get());
                    }

                    ChatController.this.clientsList.getSelectionModel().clearSelection();
                    dialog.hide();
                });
            }
        });
    }

}
